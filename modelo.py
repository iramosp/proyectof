import sys, copy, random, os, glob, math
import numpy as np
from itertools import combinations, product
from scipy.integrate import odeint
from scipy.signal import convolve2d


def correr_2DMM(poblacion_0, tipo, t_total, n_especies, f, r_alea, a_alea, m_milpa, m_intensivo, Disp, vecinos, iter_difymuerte = 1):

    """
    Correr_2DMM iteraciones migración y muerte y luego lotka:  LA QUE SE USA.
    Nota: se utilizan las variables "iter_difymuerte", "x_celdas", "y_celdas" del entorno global -> 
    hay que agregar un nuevo argumento para que "iter_difymuerte" sea var. del entorno local.
    y hay que obtener x_celdas, y_celdas del paisaje:
    """
    #corre simulacion en 2D
    x_celdas = len(tipo)
    y_celdas = len(tipo[1])

    poblacion = [poblacion_0] #inicializa array poblacion de 3 dimensiones con forma (x,y,n_especies)
    for t in range(t_total):
        temp = np.zeros_like(poblacion[-1])        
        for i in range(iter_difymuerte):
            for i in range(x_celdas): #para todo x y
                for j in range(y_celdas):
                    if tipo[i][j] == 'm': #milpa
                        temp[i][j] = muerte(poblacion[-1][i][j], m_milpa)
                    elif tipo[i][j] == 'i': #intensivo
                        temp[i][j] = muerte(poblacion[-1][i][j], m_intensivo)
                    elif tipo[i][j] == 'b':
                        temp[i][j] = poblacion[-1][i][j]
            #migracion
            #for i in range(n_especies):
                #si quieres variar la tasa de migracion por especie aqui es donde debes de variarla            
            #print tipo
            temp = migracion_esp(temp, tipo, Disp)
            poblacion.append(temp)
        #interacciones ecologicas y muerte
        for i in range(x_celdas): #para todo x y
            for j in range(y_celdas):
                if tipo[i][j] == 'b': #interacciones ecologicas
                    temp[i][j] = odeint(f, poblacion[-1][i][j], [0,1], args=(r_alea,a_alea))[-1]
        poblacion.append(temp)
    return np.array(poblacion)  


def migracion_esp(X, tp, L):
    """
    version 0.002
    **LA MIGRACION QUE SE USA AHORA**
    Funcion que asigna cuanta poblacion migra
    dependiendo del parche en el que este
    Los valores de migración están guardados en el 
    diccionario L
    L['b'] = valor bosque
    L['m'] = valor milpa 
    L['i'] = valor intensivo
    X es la matriz de dimensión 3 X(x,y,i) donde 
    la especie i-esima tiene su representación para 
    todos los parches
    t es la distribución de tipos de parche en todo el 
    espacio
    """
    s = X.shape
    #print tp
    t = np.array(tp)
    #print t
    P = np.zeros(s, dtype=float)
    G = np.zeros(s, dtype=float)
    R = np.zeros(s, dtype=float) 
    #print "Especies "
    for idx in range(s[2]):
        esp = X[:,:,idx]
        dm = esp.shape
        xesp, yesp = dm
        #print "Espacio de la especie ", idx, ": ", xesp,":",yesp
        loss_e = np.zeros( dm, dtype=float ) 
        gain_e = np.zeros ( dm, dtype=float )       
        for x in range(xesp):
            for y in range(yesp):
                #print t[x,y]
                loss_e[x,y] = esp[x,y] * L[t[x,y]]
        for x in range(-1,dm[0]):
            for y in range(-1,dm[1]):
                gain_e[x,y] = (loss_e[(x-1)%dm[0],(y-1)%dm[1]]+loss_e[(x-1)%dm[0],y%dm[1]]+loss_e[(x-1)%dm[0],(y+1)%dm[1]]+loss_e[x%dm[0],(y-1)%dm[1]]+loss_e[x%dm[0],(y+1)%dm[1]]+loss_e[(x+1)%dm[0],(y-1)%dm[1]]+loss_e[(x+1)%dm[0],y%dm[1]]+loss_e[(x+1)%dm[0],(y+1)%dm[1]])/8
        P[:,:,idx] = loss_e
        G[:,:,idx] = gain_e
        R[:,:,idx] = esp +( gain_e - loss_e )
      
    return R 

def d_lotkavolterra_alea(x, t, r_alea, a_alea): 
    """
    Ecuacion de lotka volterra generalizada ESTA ES LA QUE SE USA
    """
    dx = x * (r_alea + np.dot(a_alea, x))
    return dx 

def muerte(x, m):
    """
    Recibe  x = poblacion
            m = tasa muerte cte o np.array
    Regresa x = poblacion superviviente  np.array
    """
    x = x - x*m
    return x