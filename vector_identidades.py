import numpy as np

def vector_identidades(n_especies, a): 
    """
    Crea un vector que indica si cada especie es basal, intermedia o top.
    """
    identidades = np.zeros(n_especies) #inicia vector con ceros, terminará 0=intermedia, 1=basal, 2=top
    #Basales (sólo un -1 en la matriz de nicho original)    
    matrizbasales = a*-1  #ahora sólo no simportan los 1    
    matrizbasales[matrizbasales<=0] = 0  
    # print matrizr    
    for i in range (len(matrizbasales)):
    #    print sum(matrizr[:,i])
        if sum(matrizbasales[:,i])==1: #si son basales (solo tienen un uno)
            #print "la especie", i, "es basal"
            identidades[i]=1
        else: pass    
    #Top (al menos dos -1 y ningún 1 en la original)    
    matriztop = a*1
    matriztop[matriztop >= 0] = 0        
    for i in range (len(a)):
        if sum(a[:,i]) == sum(matriztop[:,i]):
            #print "la especie", i, "es top"
            identidades[i]=2
        else: pass    
    return identidades   