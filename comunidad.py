"""
FUNCIONES PARA GENERAR LA  COMUNIDAD
"""

def matriznicho(s_especies = 10, c_conectancia = 0.2):
    """
    Crea la matriz de interacciones con 0, 1 y -1s utilizando el modelo de nicho de Williams & Martinez, 2000.
    """
    f_matrizbuena = False
    while not f_matrizbuena:        
        ni = [random.random() for _ in xrange(s_especies)]
        array_ni = np.array(ni)
       #print array_ni        
        ci = [np.random.random()*x for x in ni]
        array_ci = np.array(ci)
       #print array_ci        
        ripre = np.random.beta(1,(1.0/(2*c_conectancia))-1,s_especies) #c_conectancia <= .5 
        array_ripre = np.array(ripre)
        #print array_ripre        
        ri = array_ripre * array_ni
        #print ri        
        rimedios = ri/2
        #print rimedios        
        extizq = array_ci - rimedios
        #print extizq        
        extder = array_ci + rimedios
       #print extder        
        redtrof = np.zeros(shape=(s_especies,s_especies)) #inicializa matriz de especies vs especies en ceros        
        for i in range(len(array_ni)): 
            for j in range(len(extizq)):
                if array_ni[i] >= extizq[j] and array_ni[i] <= extder[j]: 
                    redtrof[i,j] = -1 
                    redtrof[j,i] = 1 #aij=-1 en la matriz tr�fica si j se come a i, aji=-1
                else: pass                
        np.fill_diagonal(redtrof, -1)        #pone diagonal negativa         
        #print redtrof #devuelve la matriz tr�fica         
        #comparar filas y columnas, devuelve los pares id�nticos        
        f_identica = False        
        for i in range(len(redtrof)): #generate pairs
            for j in range(i+1,len(redtrof)): 
                if np.array_equal(redtrof[i],redtrof[j]): #compare rows
                    if np.array_equal(redtrof[:,i],redtrof[:,j]): #compare columns
                        #print (i, j)
                        f_identica = True
                else: pass                
        #devuelve especies disconexas        
        f_disconexa = False        
        for i in range(len(redtrof)): 
                if sum(np.absolute(redtrof[i])) <= 1:
                    if sum(np.absolute(redtrof[:,i])) <= 1:
                        #print i,
                        f_disconexa = True
                #else: print "cool"                
        #calcula conectancia        
        f_conectancia = False        
        links = (np.sum(np.absolute(redtrof))/2)-s_especies
        #print "links existentes", links
        conectanciae = links/s_especies**2
        #print "conectancia empirica", conectanciae        
        if np.absolute(conectanciae - c_conectancia) > .03:
            f_conectancia = True            
        if not f_identica and not f_disconexa and not f_conectancia:
            #print redtrof
            return redtrof
            f_matrizbuena=True

def a_aleatorizada(n_especies, a):
    """
    Toma la matriz trofica "a" creada con el modelo de nicho y aleatoriza sus magnitudes, conservando el signo
    """
    #global amodificada # evitar las variables globales
    amodificada = np.zeros([n_especies, n_especies]) #para hacer interacciones negativas menos fuertes cambiando -1 por algo negativo mayor
    for i in range(len(a)):
        for j in range(len(a)):
            if a[i,j]==-1:
                amodificada[i,j]=-0.1   #valor por el cual se sustituyen los -1
            else:
                amodificada[i,j]=a[i,j]  #lo dem�s se queda igual
    M = np.zeros([n_especies, n_especies])
    for i in range(len(M)):
        for j in range (len(M)):
            M[i,j] = np.random.uniform(0,2) #aleatorizados de [0,2]
#            M[i,j] = np.absolute(np.random.normal())   #aleatorizados con el valor absoluto de un aleatorio con dist normal       
    matrizaleatorizada = M*amodificada #a_alea con interacc negativas menos fuertes
#    matrizaleatorizada = M*a   #a_alea con interacciones negativas y positivas aleatorias en el mismo intervalo
    return matrizaleatorizada
    
def nicho_taza_crecimiento(a): 
    """
    Establecer tasa crecimiento dependiendo de si la especie es basal (+1) o no (-1)
    """
    r = np.empty(len(a))
    matrizr = a*-1  #ahora nos interesan los 1s en las columnas
    r.fill(-1)    #inicializa array de rs con -1s
    matrizr[matrizr<=0] = 0  # hace 0 todo lo que no son unos
    #print matrizr
    for i in range (len(matrizr)):
    #print sum(matrizr[:,i])
        if sum(matrizr[:,i])==1:    #suma los unos en cada columna
            r[i]=1                  #si s�lo hay un uno, la especie es basal, su r se hace 1
    return r        
    
def r_aleatorizado(r):   #toma el vector de r y aleatoriza sus magnitudes, conservando el signo
    raleatorio = np.empty(len(r))
    for i in range (len(r)):
        if r[i]==1:
            raleatorio[i]=r[i]*np.random.uniform(0,10) #r positivas ente 0 y 2.5
        else:
            raleatorio[i]=r[i]*np.random.random() #r negativas entre -1 y 0
#    raleatorio = [np.random.uniform(0,2.5)*x for x in r]
#    raleatorio = [np.random.normal(2.5)*x for x in r] #aleatorizado con dist normal con centro no cero
    return raleatorio