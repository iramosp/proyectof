import sys, os, glob
import numpy as np
from scipy.integrate import odeint
from matplotlib import pyplot as plt

def d_lotkavolterra_alea(x, t, r_alea, a_alea): #ecuacion de lotka volterra generalizada ESTA ES LA QUE SE USA
    dx = x * (r_alea + np.dot(a_alea, x))
    return dx

def correr_aleatorizado_sin_matriz(n_especies, t_total, f, r_alea, a_alea, plot=False): 
    """corre el sistema con la ecuacion lotka volterra
    """
    global x_0
    #x_0 = 1/(np.random.random(n_especies)) #pob inicial aleatoria mayor a 1 EL QUE USÉ
    x_0 = Nombres[actual][0] #iniciando con condiciones guardadas
    t = np.linspace(0, t_total, t_total+1)
    x = odeint(f, x_0, t, args=(r_alea,a_alea))
    if plot:
        fig = plt.figure()
        fig.add_subplot(111)
        plt.plot(t, x)
        plt.show()  
    return x[-1] #¿Esta es la población final?


n_especies = 10 # numero especies
x_celdas = 10 # numero celdas en x
y_celdas = 10 # numero celdas en y

numerodemilpas = 95

Disp = {'b':0.3, 'm':1.0, 'i':1.0} #tasas dipersión para nueva función migracion_esp

m_milpa = 0.3 #taza muerte negra cte o np.array
m_intensivo = 0.6 #taza muerte blanca cte o np.array

h = 0.001 #diferencial de cambio en t (euler y graficas)
t_total = 200 #tiempo total de simulacion
iter_difymuerte = int(sys.argv[1]) #iteraciones de dif y muerte entre cada una de lotka volterra

#now = time.strftime('%c')

"""para correr valores guardados"""
"""
carga todos los arhivos en la carpeta,
crea un diccionario Nombres donde cada entrada
contiene una lista con 3 elementos:
ci: condiciones iniciales, 
tc: tasa de crecimiento
ma: matriz de interacciones
"""
os.chdir('C:\Users\hp\Desktop\proyectof\prueba')
CI = glob.glob('*-c.txt')
Nombres = dict()
idx=1
for nci in CI:
    pn = nci[:-5]
    ntc = pn+"v.txt"
    nma = pn+"m.txt"
    ci = np.loadtxt(nci)
    tc = np.loadtxt(ntc)
    ma = np.loadtxt(nma)
    Nombres[idx] = [ci,tc,ma]
    idx += 1   
actual= int(sys.argv[2])  #número de corrida a utilizar. va de 1 a n.
print "CI", CI[actual-1]

"""corre el sistema lotka-volterra sin espacio"""
 
#x= correr_aleatorizado_sin_matriz(n_especies, t_total, d_lotkavolterra_alea, r_alea, a_alea, True) #corre la ecuación sin el espacio
x= correr_aleatorizado_sin_matriz(n_especies, t_total, d_lotkavolterra_alea, Nombres[actual][1], Nombres[actual][2], True) #la corre con valores guardados

print "sobrevivieron", ((sum([i>0.0001 for i in x]))/(n_especies*1.0))*10 #cuantas sobrevivieron con x mayor a 0.0001
for i in range(len(x)): print "total de cada especie", x[i]    #total de cada especie
print "suma total", sum(x)     #suma total