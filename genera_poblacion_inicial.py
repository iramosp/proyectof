import numpy as np
import copy, random

def genera_poblacion_inicial(tipo_matriz_agroecologica, n_especies, p0_bosque=0, p0_milpa=0, p0_intensivo=0): 
    """
    Poblacion inicial, puebla el paisaje.

    ¿Dentro de la función se hace referencia a una variable "tipo", pero no es
    una variable del entorno local de la función, sino que la toma del entorno
    del programa?
    cambiaré "tipo_matriz_agroecologica" por "tipo"

    Además, no entiendo por qué considerar diferentes condiciones float vs int
    """
    tipo = tipo_matriz_agroecologica

    if type(p0_bosque)==float: #all same value
        p0_bosque = [p0_bosque for i in range(n_especies)]
    elif type(p0_bosque)==int: #users
        p0_bosque = [p0_bosque for i in range(n_especies)]
    #elif p0_bosque=="eq_caos":p0_bosque = [ 0.3013,  0.4586,  0.1307,  0.3557]
    
    if type(p0_milpa)==float: #all same value
        p0_milpa = [p0_milpa for i in range(n_especies)]
    elif type(p0_milpa)==int: #users
        p0_milpa = [p0_milpa for i in range(n_especies)]
        
    if type(p0_intensivo)==float: #all same value
        p0_intensivo = [p0_intensivo for i in range(n_especies)]
    elif type(p0_intensivo)==int: #users
        p0_intensivo = [p0_intensivo for i in range(n_especies)]
    
    poblacion_0 = copy.deepcopy(tipo)
    for x in range(len(tipo)): #inicializar poblaciones
        for y in range(len(tipo[0])):
            ## Estas condiciones se pueden considerar al inicio, como parámetros iniciales. Es decir, 
            ## desde el inicio se elige si p0_bosque es random o valores guardados, de esta forma
            ## aquí no se necesitarían 2 if/else
            if tipo[x][y] == 'b': 
                if p0_bosque=="random": poblacion_0[x][y] = [1/random.random() for i in range(n_especies)]
                else: poblacion_0[x][y] = p0_bosque
            if tipo[x][y] == 'm': 
                if p0_milpa=="random": poblacion_0[x][y] = [random.random() for i in range(n_especies)]
                else: poblacion_0[x][y] = p0_milpa
            if tipo[x][y] == 'i': 
                if p0_intensivo=="random": poblacion_0[x][y] = [random.random() for i in range(n_especies)]              
                else: poblacion_0[x][y] = p0_intensivo
                
    #poblacion_0[1][1] = [0,0,0,0,0,0,0,0,0,0] #para vaciar los 4 bosques de las esquinas 
    #poblacion_0[1][8] = [0,0,0,0,0,0,0,0,0,0]
    #poblacion_0[8][1] = [0,0,0,0,0,0,0,0,0,0]
    #poblacion_0[8][8] = [0,0,0,0,0,0,0,0,0,0]    
                
    return np.array(poblacion_0) #array de 3 dimensiones con forma (x,y,n_especies)