#!/usr/bin/env python
# -*- coding: utf-8 -*- 

# 19 diciembre 2014, corre la ecuación sola, la corre en el espacio de la matriz y puede guardar y abrir archivos para correrlos. parece funcionar.

# Sean celdas que representan micro-ecosistemas
# Las celdas de bosque tienen un pequeño ecosistema
# Las celdas milpa son campos lindos
# Las celdas intensivo son campos feos
# Los agentes tratan de migrar a traves de las celdas

# poblacion = [tiempo] [x][y] [especieA][especieB][...]

import sys, copy, random, os, glob, math
import numpy as np
import time
from itertools import combinations, product
from scipy.integrate import odeint
from scipy.signal import convolve2d
from matplotlib import pyplot as plt
from matplotlib import colorbar as cbar
from matplotlib import colors as colors


"""
CORRIDAS
"""
def correr_aleatorizado_sin_matriz(n_especies, t_total, f, r_alea, a_alea, plot=False): 
    """
    Corre el sistema con la ecuacion lotka volterra
    """
    global x_0
    #x_0 = 1/(np.random.random(n_especies)) #pob inicial aleatoria mayor a 1 EL QUE USÉ
    x_0 = Nombres[actual][0] #iniciando con condiciones guardadas
    #x_0 = puntofijo #inciando desde los puntos fijos
    t = np.linspace(0,t_total,t_total+1)
    x = odeint(f, x_0, t, args=(r_alea,a_alea))
    if plot:
        fig = plt.figure()
        fig.add_subplot(111)
        plt.plot(t, x)
        plt.show()  
    return x[-1]
    
def correr_2DMM(poblacion_0, tipo, t_total, n_especies, f, r_alea, a_alea, m_milpa, m_intensivo, D, vecinos):
    """
    Correr_2DMM iteraciones migración y muerte y luego lotka:  LA QUE SE USA.
    """
    #corre simulacion en 2D
    poblacion = [poblacion_0] #inicializa array poblacion de 3 dimensiones con forma (x,y,n_especies)
    for t in range(t_total):
        temp = np.zeros_like(poblacion[-1])        
        for i in range(iter_difymuerte):
            for i in range(x_celdas): #para todo x y
                for j in range(y_celdas):
                    if tipo[i][j] == 'm': #milpa
                        temp[i][j] = muerte(poblacion[-1][i][j], m_milpa)
                    elif tipo[i][j] == 'i': #intensivo
                        temp[i][j] = muerte(poblacion[-1][i][j], m_intensivo)
                    elif tipo[i][j] == 'b':
                        temp[i][j] = poblacion[-1][i][j]
            #migracion
            #for i in range(n_especies):
                #si quieres variar la tasa de migracion por especie aqui es donde debes de variarla            
            temp = migracion_esp(temp, tipo, Disp)
            poblacion.append(temp)
        #interacciones ecologicas y muerte
        for i in range(x_celdas): #para todo x y
            for j in range(y_celdas):
                if tipo[i][j] == 'b': #interacciones ecologicas
                    temp[i][j] = odeint(f, poblacion[-1][i][j], [0,1], args=(r_alea,a_alea))[-1]
        poblacion.append(temp)
    return np.array(poblacion)  

"""
VALORES INICIALES
"""

"""
FUNCIONES
"""

def d_lotkavolterra_alea(x, t, r_alea, a_alea): 
    """
    Ecuacion de lotka volterra generalizada ESTA ES LA QUE SE USA
    """
    dx = x * (r_alea + np.dot(a_alea, x))
    return dx 

def muerte(x, m):
    """
    Recibe  x = poblacion
            m = tasa muerte cte o np.array
    Regresa x = poblacion superviviente  np.array
    """
   x = x - x*m
   return x

"""
M   M     A  III  N   N
MM MM    AA   I   NN  N
M M M   A A   I   N N N
M   M  AAAA   I   N  NN
M   M A   A  III  N   N
"""

"""
PARAMETROS
"""

"""
n_especies = 10 # numero especies
x_celdas = 10 # numero celdas en x
y_celdas = 10 # numero celdas en y

# Crear comunidad

a = matriznicho(n_especies) #matriz de interacciones antes de modificar
a_alea = a_aleatorizada(n_especies, a) # matriz de interacciones que se usa
r = nicho_taza_crecimiento(a) #tasa crecimiento de las especies antes de modificar
r_alea = r_aleatorizado(r) # tasa de crecimiento que se usa

numerodemilpas = 95

##identidades = vector_identidades(n_especies, a)

Disp = {'b':0.3, 'm':1.0, 'i':1.0} #tasas dipersión para nueva función migracion_esp

m_milpa = 0.3 #taza muerte negra cte o np.array (//¿por qué o para qué esto puede ser un arreglo?//)
m_intensivo = 0.6 #taza muerte blanca cte o np.array (//¿por qué o para qué esto puede ser un arreglo?//)

h = 0.001 #diferencial de cambio en t (euler y graficas)
t_total = 1000 #tiempo total de simulacion
#iter_difymuerte = int(sys.argv[1]) #iteraciones de dif y muerte entre cada una de lotka volterra
iter_difymuerte = 1 #iteraciones de dif y muerte entre cada una de lotka volterra
print "iteraciones", iter_difymuerte

"""

"""
CORRIDAS
"""


"""corre el sistema lotka-volterra sin espacio"""
 """
#x= correr_aleatorizado_sin_matriz(n_especies, t_total, d_lotkavolterra_alea, r_alea, a_alea, True) #corre la ecuación sin el espacio
x= correr_aleatorizado_sin_matriz(n_especies, t_total, d_lotkavolterra_alea, Nombres[actual][1], Nombres[actual][2], True) #la corre con valores guardados
print ((sum([i>0.0001 for i in x]))/(n_especies*1.0))*10 #cuantas sobrevivieron con x mayor a 0.0001
for i in range(len(x)): print x[i]    #total de cada especie
print sum(x)     #suma total

 """    


"""Graficas varias"""

#heatmap_tipo('tipo.png',tipo)
#heatmaps_especie_en_tiempo('especie.png',poblacion, labels = map(str, range(n_especies)))

#sum_xy = np.sum(np.sum(poblacion, axis=1),axis=1) #calcula sum en xy para cada especie
#plot_lineas_especies('sum_xy.png', sum_xy, t_total, labels=map(str, range(n_especies)))
#print "sumxy:", sum_xy[-1]


"""
AQUI EMPIEZA; EL EXPERIMENTO
"""

"""
#para correr valores guardados:

#carga todos los arhivos en la carpeta,
#crea un diccionario Nombres donde cada entrada
#contiene ci: condiciones iniciales, 
#tc: tasa de crecimiento
#ma: matriz de interacciones
"""

now = time.strftime('%c')

os.chdir('/Users/personal2/Desktop/equilibrio')
CI = glob.glob('*-c.txt')
Nombres = dict()
idx=1
for nci in CI:
    pn = nci[:-5]
    ntc = pn+"v.txt"
    nma = pn+"m.txt"
    ci = np.loadtxt(nci)
    tc = np.loadtxt(ntc)
    ma = np.loadtxt(nma)
    Nombres[idx] = [ci,tc,ma]
    idx += 1   
actual = int(sys.argv[2])  #número de comunidad a utilizar. va de 1 a n.
#actual= 1
print "actual", actual
print CI[actual-1] #escribe nombre del archivo abierto
print "de 95 a 0"


for n in range(20):
    # Generar el paisaje
    tipo = genera_tipo_matriz_agroecologica(x_celdas, y_celdas, n_bosque=5, posicion_bosque=[(1,1),(1,8),(4,5),(8,1),(8,8)], n_milpa=numerodemilpas, posicion_milpa="random")

    # Poblar el paisaje
    poblacion_0 = genera_poblacion_inicial(tipo, n_especies, p0_bosque = Nombres[actual][0], p0_milpa=0, p0_intensivo=0)
    
    # Correr el modelo
    poblacion = correr_2DMM(poblacion_0, tipo, t_total, n_especies, d_lotkavolterra_alea, Nombres[actual][1], Nombres[actual][2], m_milpa, m_intensivo, Disp, 'vecinos8')

    #aquí pegar medidas para equilibrio o ciclo, según el caso  
    
      
    riqueza=[]
    for idx in range(n_especies):
        #print np.sum(poblacion[-1,:,:,idx]) #suma de cada especie en la última iteración
        if np.sum(poblacion[-1,:,:,idx])>0.0001:
            riqueza.append(1)  #vivos arriba de 0.0001   
    print  np.sum(poblacion[-1,:,:,:]) #suma total de individuos al final de n iteraciones
    #sumbosques=np.sum(poblacion[-1,1,1,:])+np.sum(poblacion[-1,1,8,:])+np.sum(poblacion[-1,5,5,:])+np.sum(poblacion[-1,8,1,:])+np.sum(poblacion[-1,8,8,:]) #suma total en los 5 bosques en la última iteración
    #print sumbosques #sum(x) bosques
    #print np.sum(poblacion[-1,:,:,:])-sumbosques #sum(x) matriz
    print np.sum(riqueza) #riqueza de especies al final de n iteraciones
    ##shannon = alpha_shannon(poblacion[-1])
    ##print "shannon", shannon
    ##print "nefec", nefectivo(shannon)
    #
    #
    ##riqueza promedio en bosques y matriz y alfa y beta 
    #riqb=[]
    #riqm=[]
    #for i in range(len(tipo)):
    #    for j in range(len(tipo)):
    #        if tipo[i][j] == 'b':
    #            riquezas=(poblacion[-1,i,j,:]>0.0001).sum()
    #            riqb.append(riquezas)
    #        else:
    #            riquezas2=(poblacion[-1,i,j,:]>0.0001).sum()
    #            riqm.append(riquezas2)
    #print np.mean(riqb) #riqueza promedio en bosques
    #print np.mean(riqm) #riqueza promedio en matriz
    #promtot=float(np.sum(riqb)+np.sum(riqm))/(x_celdas*y_celdas)
    #print promtot #riqueza promedio total
    ##print "riq beta", np.sum(riqueza)/promtot #riqueza beta
    
    #riqueza=[]
    #promedios=[]
    #for idx in range(n_especies):
    #    promedio=np.mean(np.sum(np.sum(poblacion[-200:,:,:,idx],1),1)) #promedio de cada especie en las últimas 200 iteraciones
    #    #print promedio
    #    promedios.append(promedio) #pone el promedio de cada especie en una lista
    #    if np.mean(np.sum(np.sum(poblacion[-200:,:,:,idx],1),1))>0.0001: riqueza.append(1)  #si el promedio de cada sp está arriba de 0.0001, agrega 1 a la lista "riqueza"
    #print  np.sum(promedios) #suma promedio de las especies con las últimas 200 iteraciones
    #sumbosques=np.mean(np.sum(poblacion[-200:,1,1,:],1))+np.mean(np.sum(poblacion[-200:,1,8,:],1))+np.mean(np.sum(poblacion[-200:,5,5,:],1))+np.mean(np.sum(poblacion[-200:,8,1,:],1))+np.mean(np.sum(poblacion[-200:,8,8,:],1)) #suma total en los 5 bosques en la última iteración
    #print sumbosques #sum(x) bosques
    #print np.sum(promedios)-sumbosques #sum(x) matriz
    #print np.sum(riqueza) 
    ##poblacionpromedio=np.mean(poblacion[-200:,:,:,:],0) #promedia sobre eje tiempo: arroja array de forma (10,10,10) con las poblaciones promedio de las últimas 200 iteraciones
    ##shannon = alpha_shannon(poblacionpromedio)
    ##print "shannon", shannon
    ##print "nefectivo", nefectivo(shannon)
    #
    ##riqueza promedio en bosques y matriz y alfa y beta 
    #riqb=[] #aquí irá la riqueza en cada celda bosque para depués promediar
    #riqm=[] #aquí irá la riqueza en cada celda matriz para depués promediar
    #for i in range(len(tipo)):
    #    for j in range(len(tipo)):
    #        if tipo[i][j] == 'b':
    #            riquezas=(np.mean(poblacion[-200:,i,j,:],0)>0.001).sum() #usa array promediado en eje tiempo
    #            riqb.append(riquezas)
    #        else:
    #            riquezas2=(np.mean(poblacion[-200:,i,j,:],0)>0.001).sum() #usa array promediado en eje tiempo
    #            riqm.append(riquezas2)
    #print np.mean(riqb) #riqueza promedio en bosques
    #print np.mean(riqm) #riqueza promedio en matriz
    #promtot=float(np.sum(riqb)+np.sum(riqm))/(x_celdas*y_celdas)
    #print promtot #riqueza promedio total
    ##print np.sum(riqueza)/promtot #riqueza beta
    #
    #
    
    numerodemilpas-=5

"""