import copy, math, random
import numpy as np
from scipy.integrate import odeint

def modelo_v2(paisaje, matriz_interacciones, tasas_reproduccion, condiciones_iniciales, t_total, Manejos):
    """
    Segunda versión del modelo. Muerte diferencial, ocurre dinámica de poblaciones en bosque y milpa.
    """

    x_celdas = len(paisaje)
    y_celdas = len(paisaje[1])
    n_especies = len(matriz_interacciones)

    # Tasas dispersión
    Disp = {"b": round((0.7*Manejos["b"] + 2) /9, 1),
            "m": round((0.7*Manejos["m"] + 2) /9, 1),
            "i": round((0.7*Manejos["i"] + 2) /9, 1)}
    
    # Inicializar un arreglo de numpy de la forma poblacion = [tiempo] [x][y] [especieA][especieB][...]
    poblacion = np.zeros((t_total + 1, x_celdas, y_celdas, n_especies))

    # Población inicial con las condiciones iniciales en las celdas de bosque
    poblacion[0,:,:,:] = genera_poblacion_inicial(paisaje, n_especies, p0_bosque = condiciones_iniciales, p0_milpa=0, p0_intensivo=0)

    for t in range(1, t_total+1):
        poblacion[t, :, :, :] = copy.deepcopy(poblacion[t-1]) # la poblacíón en el tiempo anterior 

        for k in range(6):
            # Migración
            poblacion[t, :, :, :] = migracion_esp(poblacion[t, :, :, :], paisaje, Disp)
        
            # Muerte
            for i in range(x_celdas): #para todo x y
                for j in range(y_celdas):
                    tasa_muerte = 0.1 * (Manejos[paisaje[i][j]] - 1)
                    poblacion[t, i, j, :] = (1-tasa_muerte) * poblacion[t, i, j, :]
            

        # Lotka - dinámica de poblaciones
        for i in range(x_celdas): #para todo x y
            for j in range(y_celdas):
                if paisaje[i][j] != "i":
                    # Ocurren interacciones ecológicas en bosque y milpa
                    manejo_numerico = -0.1 * Manejos[paisaje[i][j]] + 1.1
                    poblacion[t, i, j, :] = odeint(lotka1, poblacion[t, i, j, :], [0, 0.5], 
                                            args=(tasas_reproduccion, matriz_interacciones, manejo_numerico))[-1]
    return poblacion


def migracion_esp(X, tp, L):
    """
    version 0.002
    **LA MIGRACION QUE SE USA AHORA**
    Funcion que asigna cuanta poblacion migra
    dependiendo del parche en el que este
    Los valores de migración están guardados en el 
    diccionario L
    L['b'] = valor bosque
    L['m'] = valor milpa 
    L['i'] = valor intensivo
    X es la matriz de dimensión 3 X(x,y,i) donde 
    la especie i-esima tiene su representación para 
    todos los parches
    t es la distribución de paisajes de parche en todo el 
    espacio
    """
    s = X.shape
    #print tp
    t = np.array(tp)
    #print t
    P = np.zeros(s, dtype=float)
    G = np.zeros(s, dtype=float)
    R = np.zeros(s, dtype=float) 
    #print "Especies "
    for idx in range(s[2]):
        esp = X[:,:,idx]
        dm = esp.shape
        xesp, yesp = dm
        #print "Espacio de la especie ", idx, ": ", xesp,":",yesp
        loss_e = np.zeros( dm, dtype=float ) 
        gain_e = np.zeros ( dm, dtype=float )       
        for x in range(xesp):
            for y in range(yesp):
                #print t[x,y]
                loss_e[x,y] = esp[x,y] * L[t[x,y]]
        for x in range(-1,dm[0]):
            for y in range(-1,dm[1]):
                gain_e[x,y] = (loss_e[(x-1)%dm[0],(y-1)%dm[1]]+loss_e[(x-1)%dm[0],y%dm[1]]+loss_e[(x-1)%dm[0],(y+1)%dm[1]]+loss_e[x%dm[0],(y-1)%dm[1]]+loss_e[x%dm[0],(y+1)%dm[1]]+loss_e[(x+1)%dm[0],(y-1)%dm[1]]+loss_e[(x+1)%dm[0],y%dm[1]]+loss_e[(x+1)%dm[0],(y+1)%dm[1]])/8
        P[:,:,idx] = loss_e
        G[:,:,idx] = gain_e
        R[:,:,idx] = esp +( gain_e - loss_e )
      
    return R 

def lotka1(x, t, r, a, manejo): 
    """
    Ecuacion de lotka volterra generalizada que incorpora la intensidad de manejo.
    **De esta forma, la población crece en forma proporcional al manejo.
    """
    r = r / manejo
    r[r > 0] = r[r > 0] * (manejo) ** 2
    return (x * (r + np.dot(a, x)))

def genera_poblacion_inicial(tipo_matriz_agroecologica, n_especies, p0_bosque=0, p0_milpa=0, p0_intensivo=0): 
    """
    Poblacion inicial, puebla el paisaje.

    ¿Dentro de la función se hace referencia a una variable "tipo", pero no es
    una variable del entorno local de la función, sino que la toma del entorno
    del programa?
    cambiaré "tipo_matriz_agroecologica" por "tipo"

    Además, no entiendo por qué considerar diferentes condiciones float vs int
    """
    tipo = tipo_matriz_agroecologica

    if type(p0_bosque)==float: #all same value
        p0_bosque = [p0_bosque for i in range(n_especies)]
    elif type(p0_bosque)==int: #users
        p0_bosque = [p0_bosque for i in range(n_especies)]
    #elif p0_bosque=="eq_caos":p0_bosque = [ 0.3013,  0.4586,  0.1307,  0.3557]
    
    if type(p0_milpa)==float: #all same value
        p0_milpa = [p0_milpa for i in range(n_especies)]
    elif type(p0_milpa)==int: #users
        p0_milpa = [p0_milpa for i in range(n_especies)]
        
    if type(p0_intensivo)==float: #all same value
        p0_intensivo = [p0_intensivo for i in range(n_especies)]
    elif type(p0_intensivo)==int: #users
        p0_intensivo = [p0_intensivo for i in range(n_especies)]
    
    poblacion_0 = copy.deepcopy(tipo)
    for x in range(len(tipo)): #inicializar poblaciones
        for y in range(len(tipo[0])):
            ## Estas condiciones se pueden considerar al inicio, como parámetros iniciales. Es decir, 
            ## desde el inicio se elige si p0_bosque es random o valores guardados, de esta forma
            ## aquí no se necesitarían 2 if/else
            if tipo[x][y] == 'b': 
                if p0_bosque=="random": poblacion_0[x][y] = [1/random.random() for i in range(n_especies)]
                else: poblacion_0[x][y] = p0_bosque
            if tipo[x][y] == 'm': 
                if p0_milpa=="random": poblacion_0[x][y] = [random.random() for i in range(n_especies)]
                else: poblacion_0[x][y] = p0_milpa
            if tipo[x][y] == 'i': 
                if p0_intensivo=="random": poblacion_0[x][y] = [random.random() for i in range(n_especies)]              
                else: poblacion_0[x][y] = p0_intensivo
                
    #poblacion_0[1][1] = [0,0,0,0,0,0,0,0,0,0] #para vaciar los 4 bosques de las esquinas 
    #poblacion_0[1][8] = [0,0,0,0,0,0,0,0,0,0]
    #poblacion_0[8][1] = [0,0,0,0,0,0,0,0,0,0]
    #poblacion_0[8][8] = [0,0,0,0,0,0,0,0,0,0]    
                
    return np.array(poblacion_0) #array de 3 dimensiones con forma (x,y,n_especies)