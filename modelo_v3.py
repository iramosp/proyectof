import copy, math, random
import numpy as np
from scipy.integrate import odeint

def modelo_v3(paisaje, matriz_interacciones, tasas_reproduccion, condiciones_iniciales, t_total, Manejos, pasos_mm=2):
    """
    Tercera versión del modelo. Muerte diferencial, ocurre dinámica de poblaciones en bosque y milpa
    LV antes de migración-muerte.
    Se guarda población en cada paso del modelo.
    """
    x_celdas = len(paisaje)
    y_celdas = len(paisaje[1])
    n_especies = len(matriz_interacciones)

    # Tasas dispersión
    Disp = {"b": round((0.7*Manejos["b"] + 2) /9, 1),
            "m": round((0.7*Manejos["m"] + 2) /9, 1),
            "i": round((0.7*Manejos["i"] + 2) /9, 1)}
    
    # Inicializar un arreglo de numpy de la forma poblacion = [tiempo] [x][y] [especieA][especieB][...]
    poblacion = np.zeros(((pasos_mm +1 ) * t_total + 1, x_celdas, y_celdas, n_especies))

    # Población inicial con las condiciones iniciales en las celdas de bosque   
    poblacion[0,:,:,:] = genera_poblacion_inicial(paisaje, n_especies, p0_bosque = condiciones_iniciales)

    for t in range(1, t_total+1):
        T = (pasos_mm + 1) * t - pasos_mm

        # Dinámica de poblaciones en bosque y milpa
        for i in range(x_celdas): #para todo x y
            for j in range(y_celdas):
                if paisaje[i][j] != "i":
                    manejo_numerico = -0.1 * Manejos[paisaje[i][j]] + 1.1

                    poblacion[T, i, j, :] = odeint(lotka1, poblacion[T-1, i, j, :], [0, 0.5], 
                                            args=(tasas_reproduccion, matriz_interacciones, manejo_numerico))[-1]
    
    
        for k in range(1, pasos_mm + 1):
            # Migración
            poblacion[T + k, :, :, :] = migracion_esp(poblacion[T + k -1, :, :, :] , paisaje, Disp)

            # Muerte
            for i in range(x_celdas): #para todo x y
                for j in range(y_celdas):
                    tasa_muerte = 0.1 * (Manejos[paisaje[i][j]] - 1)
                    poblacion[T + k, i, j, :] = (1-tasa_muerte) * poblacion[T + k, i, j, :]

    return poblacion


def migracion_esp(X, tp, L):
    """
    version 0.002
    **LA MIGRACION QUE SE USA AHORA**
    Funcion que asigna cuanta poblacion migra
    dependiendo del parche en el que este
    Los valores de migración están guardados en el 
    diccionario L
    L['b'] = valor bosque
    L['m'] = valor milpa 
    L['i'] = valor intensivo
    X es la matriz de dimensión 3 X(x,y,i) donde 
    la especie i-esima tiene su representación para 
    todos los parches
    t es la distribución de paisajes de parche en todo el 
    espacio
    """
    s = X.shape
    t = np.array(tp)
    P = np.zeros(s, dtype=float)
    G = np.zeros(s, dtype=float)
    R = np.zeros(s, dtype=float) 
    for idx in range(s[2]):
        esp = X[:,:,idx]
        dm = esp.shape
        xesp, yesp = dm
        loss_e = np.zeros( dm, dtype=float ) 
        gain_e = np.zeros ( dm, dtype=float )       
        for x in range(xesp):
            for y in range(yesp):
                loss_e[x,y] = esp[x,y] * L[t[x,y]]
        for x in range(-1,dm[0]):
            for y in range(-1,dm[1]):
                gain_e[x,y] = (loss_e[(x-1)%dm[0],(y-1)%dm[1]]+loss_e[(x-1)%dm[0],y%dm[1]]+loss_e[(x-1)%dm[0],(y+1)%dm[1]]+loss_e[x%dm[0],(y-1)%dm[1]]+loss_e[x%dm[0],(y+1)%dm[1]]+loss_e[(x+1)%dm[0],(y-1)%dm[1]]+loss_e[(x+1)%dm[0],y%dm[1]]+loss_e[(x+1)%dm[0],(y+1)%dm[1]])/8
        P[:,:,idx] = loss_e
        G[:,:,idx] = gain_e
        R[:,:,idx] = esp +( gain_e - loss_e )
      
    return R 

def lotka1(x, t, r, a, manejo): 
    """
    Ecuacion de lotka volterra generalizada que incorpora la intensidad de manejo.
    **De esta forma, la población crece en forma proporcional al manejo.
    """
    r = r / manejo
    r[r > 0] = r[r > 0] * (manejo) ** 2
    return (x * (r + np.dot(a, x)))

def genera_poblacion_inicial(paisaje, n_especies, p0_bosque, p0_milpa=0, p0_intensivo=0): 
    """
    Genera la poblacion inicial. Se especifican condiciones iniciales para cada especie, para las celdas de "bosque". Las celdas "milpa" e "intensivo" inician vacías.
    Recibe: 
        paisaje: lista de x por y, especifica el tipo de celda ("b" = bosque, "m" = milpa, "i" = intensivo)
        n_especies: int 
        p0_bosque: condiciones iniciales para las celdas "bosque". Es numpy array de 1D con forma (n_especies, ).

    Regresa:
        poblacion_0: numpy array de 3D con forma (x, y, n_especies).

    """

    p0_milpa = [p0_milpa for i in range(n_especies)]
    p0_intensivo = [p0_intensivo for i in range(n_especies)]
    
    poblacion_0 = copy.deepcopy(paisaje)
    for x in range(len(paisaje)): #inicializar poblaciones
        for y in range(len(paisaje[0])):
            if paisaje[x][y] == 'b': 
                poblacion_0[x][y] = p0_bosque

            if paisaje[x][y] == 'm': 
                poblacion_0[x][y] = p0_milpa
           
            if paisaje[x][y] == 'i': 
                poblacion_0[x][y] = p0_intensivo
                
    return np.array(poblacion_0)