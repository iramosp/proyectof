import numpy as np

def vector_identidades_2(matriz_interacciones):
    """Dada una matriz de interacciones tróficas (un arreglo 2D de numpy), 
    identificar especies basales, intermedias y top.
    Entra: matriz de interacciones tróficas, que es una matriz cuadrada
    Sale: vector con 0=intermedia, 1=basal, 2=top
    """
    # Convertir a matriz de -1, 0 y 1 para usar función vector_identidades()
    a = np.copy(matriz_interacciones)
    interacciones_negativas = a[:, :] < 0.0
    interacciones_positivas = a[:, :] > 0.0
    
    # Hacer esto modifica "matriz_interacciones" en el entorno global,
    # quizá porque no es una copia, sino la misma matriz con otro nombre??
    # hice: a = np.copy(matriz_interacciones)

    a[interacciones_negativas] = -1
    a[interacciones_positivas] = 1
    
    #a = matriz_interacciones
    n_especies = a.shape[0]
    
    # Lo que sigue es el código original en la función vector_identidades()
    
    identidades = np.zeros(n_especies) #inicia vector con ceros, 
                                       #terminará 0=intermedia, 1=basal, 2=top
    #Basales (sólo un -1 en la matriz de nicho original)    
    matrizbasales = a*-1  #ahora sólo no simportan los 1    
    matrizbasales[matrizbasales<=0] = 0  
    # print matrizr    
    for i in range (len(matrizbasales)):
    #    print sum(matrizr[:,i])
        if sum(matrizbasales[:,i])==1: #si son basales (solo tienen un uno)
            #print "la especie", i, "es basal"
            identidades[i]=1
        else: pass    
    #Top (al menos dos -1 y ningún 1 en la original)    
    matriztop = a*1
    matriztop[matriztop >= 0] = 0        
    for i in range (len(a)):
        if sum(a[:,i]) == sum(matriztop[:,i]):
            #print "la especie", i, "es top"
            identidades[i]=2
        else: pass    
    
    return identidades