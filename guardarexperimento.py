def guardarexperimento(momento, m1, v1, ci):
    """
    Esta función guarda tres archivos con el nombre
    momento-{m, v, c}.txt
    usando la función savetxt de numpy. 
    
    Para cargarlos hay que usar la función, también de numpy, loadtxt
    M = np.loadtxt('ejemplo.txt')
    Entonces, en la variable M contiene lo leído.
    //¿Dónde se usa?
    """
    pref = '-'.join(momento.split())
    print 'Abriendo archivo para guardar datos'
    narch = pref+'-m.txt'
    fh = open( narch, 'w' )
    fh.write( '#Matriz \n')
    np.savetxt(fh, m1)
    fh.close()
    narch = pref+'-v.txt'
    fh = open( narch, 'w' )
    fh.write( '#Tasas de reproduccion \n' )
    np.savetxt(fh, v1)
    fh.close()
    narch = pref+'-c.txt'
    fh = open( narch, 'w' )
    fh.write( '#Condiciones iniciales\n' )
    np.savetxt(fh, ci)
    fh.close()
    print 'Datos guardados en ', narch