"""
GRAFICAS
"""
def plot_xy(name, x, y, title='',labels=[], x_label='', y_label=''): #grafica x contra y
    #recibe x como numpy.array
    #Ejemplo imprimir poblacion vs tiempo
    #plot_xy(name, x, t, title,labels, x_label='Tiempo', y_label='Poblacion')
    plt.clf()
    try:
        for i in range(len(x[0])): #grafica cada x contra y
        #print i, labels[i]
            plt.plot(y, x[:,i], label=labels[i])
    except: plt.plot(y, x)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.legend(loc=0)
    plt.plot()
    #save plot
    f_format = name.split('.')[-1]
    name = name.split('.')[0]
    plt.savefig(name+'.'+f_format, format=f_format, bbox_inches='tight')
    plt.show()

def plotHeatmap(name, data , x_label='' , y_label='', x_tick_labels=[], y_tick_labels=[]):
    #data is a 2x2 array normalized [0,1]
    plt.clf()
    fig, ax = plt.subplots()
    #delete top and right axis
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()
    ## put the major ticks at the middle of each cell
    ax.set_xticks(np.arange(data.shape[1])+0.5, minor=False)
    ax.set_yticks(np.arange(data.shape[0])+0.5, minor=False)
    ## want a more natural, table-like display
    ##ax.invert_yaxis()
    ##ax.xaxis.tick_top()
    ax.set_xticklabels(x_tick_labels, rotation=90, minor=False)
    ax.set_yticklabels(y_tick_labels, minor=False)
    #set colorbar
    cdict = {'red':   [(0.0,  1.0, 1.0),(0.01,  0.5, 0.5),(0.5,  0.0, 0.0),(1.0,  0.0, 0.0)],
        'green': [(0.0,  1.0, 1.0),(0.1, 1.0, 1.0),(1.0,  0.0, 0.0)],
        'blue':  [(0.0,  1.0, 1.0),(0.5,  1.0, 1.0),(1.0,  0.5, 0.5)]}
    my_cmap=colors.LinearSegmentedColormap('my_colormap',cdict,256)
    #heatmap = ax.pcolor(data, cmap=plt.cm.Blues)
    heatmap = ax.pcolor(data, cmap=my_cmap, vmin=0, vmax=1)
    cbar = plt.colorbar(heatmap)
    plt.title(name)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot()
    #save plot
    f_format = name.split('.')[-1]
    name = name.split('.')[0]
    plt.savefig(name+'.'+f_format, format=f_format, bbox_inches='tight')
    #plt.show()

def plot_lineas_especies(name, x, t_total, labels=[]):
    #graficar sum xy para todas las especies
    #print np.shape(labels)
    t = np.linspace(0,t_total+1,t_total+1)
    plot_xy(name, x, t, 'especies_vs_tiempo', labels)

def plot_diagramas_fase(name, x, t_total, labels=[]):
    t = np.linspace(0,t_total+1,t_total+1)
    for i,j in combinations ([n for n in range(len(x[0]))],2):
        plot_xy(name.split('.')[0] + '_'+labels[i]+labels[j]+'.' + name.split('.')[-1], x[:,i], x[:,j], 'fase_'+labels[i]+labels[j])
    
def heatmaps_especie_en_tiempo(name,poblacion, n=[], labels=[]):
    ter = '.'+name.split('.')[-1]
    name = name.split('.')[0]
    #graficar heatmap especie en un tiempo dado
    #n debe ser un array
    if type(n) == int: n = [n]
    for i in range(n_especies):
        plotHeatmap(name+label[i]+'_t0'+ter, poblacion[0,:,:,i]) #estado inicial
        plotHeatmap(name+label[i]+'_tf'+ter, poblacion[-1,:,:,i]) #estado final
        for j in n: #grafica vector de tiempos
            plotHeatmap(name+label+'_t'+str(j)+ter, poblacion[j,:,:,i])

def heatmap_tipo(name,tipo,m_milpa=.5,m_intensivo=1):
    for i in range(len(tipo)):
        for j in range(len(tipo[0])):
            if tipo[i][j] == 'b': tipo[i][j] = 1.
            if tipo[i][j] == 'm': tipo[i][j] = 1-m_milpa
            if tipo[i][j] == 'i': tipo[i][j] = 1-m_intensivo
    tipo = np.array(tipo)
    plotHeatmap(name, tipo) #estado inicial    
"""