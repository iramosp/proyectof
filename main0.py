"""AQUI EMPIEZA"""
import numpy as np
## Parámetros ##

n_especies = 10 # numero especies
x_celdas = 10 # numero celdas en x
y_celdas = 10 # numero celdas en y

"""
a = matriznicho(n_especies) #matriz de interacciones - antes de modificar
a_alea = a_aleatorizada(n_especies, a) #matriz de interacciones que se usa
r = nicho_taza_crecimiento(a) #tasa crecimiento de las especies -antes d emodificar
r_alea = r_aleatorizado(r) # tasa de crecimiento que se usa
"""
numerodemilpas = 95

Disp = {'b':0.3, 'm':1.0, 'i':1.0} #tasas dipersión se usa en la función migracion_esp

m_milpa = 0.3 #tasa muerte cte o np.array
m_intensivo = 0.6 #tasa muerte cte o np.array

h = 0.001 #diferencial de cambio en t (euler y graficas)
t_total = 200 #tiempo total de simulacion
#iter_difymuerte = int(sys.argv[1]) #iteraciones de dif y muerte entre cada una de lotka volterra
iter_difymuerte = 1
print "iteraciones", iter_difymuerte

#para correr valores guardados:
"""
now = time.strftime('%c')

os.chdir('C:\Users\hp\Desktop\proyectof\prueba')
CI = glob.glob('*-c.txt')
Nombres = dict()
idx=1
for nci in CI:
    pn = nci[:-5]
    ntc = pn+"v.txt"
    nma = pn+"m.txt"
    ci = np.loadtxt(nci)
    tc = np.loadtxt(ntc)
    ma = np.loadtxt(nma)
    Nombres[idx] = [ci,tc,ma]
    idx += 1   
actual = int(sys.argv[2])  #número de comunidad a utilizar. va de 1 a n.
#actual= 1
print "actual", actual
print CI[actual-1] #escribe nombre dle archivo abierto
print "de 95 a 0"

condicion_inicial_poblacion = Nombres[actual][0]
tasa_crecimiento = Nombres[actual][1]
matriz_interacciones = Nombres[actual][2]
"""
condicion_inicial_poblacion = np.loadtxt('C:\Users\hp\Desktop\proyectof\prueba\Fri-Jan-30-112919-2015-c.txt')
tasa_crecimiento = np.loadtxt('C:\Users\hp\Desktop\proyectof\prueba\Fri-Jan-30-112919-2015-v.txt')
matriz_interacciones = np.loadtxt('C:\Users\hp\Desktop\proyectof\prueba\Fri-Jan-30-112919-2015-m.txt')


tipo = genera_tipo_matriz_agroecologica(x_celdas, y_celdas, n_bosque=5, posicion_bosque=[(1,1),(1,8),(4,5),(8,1),(8,8)], n_milpa=numerodemilpas, posicion_milpa="random")

poblacion_0 = genera_poblacion_inicial(tipo, n_especies, p0_bosque = condicion_inicial_poblacion, p0_milpa=0, p0_intensivo=0)
                
poblacion = correr_2DMM(poblacion_0, tipo, t_total, n_especies, d_lotkavolterra_alea, tasa_crecimiento, matriz_interacciones, m_milpa, m_intensivo, Disp, 'vecinos8')

"""
for n in range(20):

    #cada ciclo es un nivel de intensidad

    tipo = genera_tipo_matriz_agroecologica(x_celdas, y_celdas, n_bosque=5, posicion_bosque=[(1,1),(1,8),(4,5),(8,1),(8,8)], n_milpa=numerodemilpas, posicion_milpa="random")

    poblacion_0 = genera_poblacion_inicial(tipo, n_especies, p0_bosque = condicion_inicial_poblacion, p0_milpa=0, p0_intensivo=0)
                
    poblacion = correr_2DMM(poblacion_0, tipo, t_total, n_especies, d_lotkavolterra_alea, tasa_crecimiento, matriz_interacciones, m_milpa, m_intensivo, Disp, 'vecinos8')

    #aquí pegar medidas para equilibrio o ciclo, según el caso  
      
    riqueza=[]
    for idx in range(n_especies):
        #print np.sum(poblacion[-1,:,:,idx]) #suma de cada especie en la última iteración
        if np.sum(poblacion[-1,:,:,idx])>0.0001: riqueza.append(1)  #vivos arriba de 0.0001   
    print  np.sum(poblacion[-1,:,:,:]) #suma total de individuos al final de n iteraciones
    #sumbosques=np.sum(poblacion[-1,1,1,:])+np.sum(poblacion[-1,1,8,:])+np.sum(poblacion[-1,5,5,:])+np.sum(poblacion[-1,8,1,:])+np.sum(poblacion[-1,8,8,:]) #suma total en los 5 bosques en la última iteración
    #print sumbosques #sum(x) bosques
    #print np.sum(poblacion[-1,:,:,:])-sumbosques #sum(x) matriz
    print np.sum(riqueza) #riqueza de especies al final de n iteraciones
    ##shannon = alpha_shannon(poblacion[-1])
    ##print "shannon", shannon
    ##print "nefec", nefectivo(shannon)
    #
    #
    ##riqueza promedio en bosques y matriz y alfa y beta 
    #riqb=[]
    #riqm=[]
    #for i in range(len(tipo)):
    #    for j in range(len(tipo)):
    #        if tipo[i][j] == 'b':
    #            riquezas=(poblacion[-1,i,j,:]>0.0001).sum()
    #            riqb.append(riquezas)
    #        else:
    #            riquezas2=(poblacion[-1,i,j,:]>0.0001).sum()
    #            riqm.append(riquezas2)
    #print np.mean(riqb) #riqueza promedio en bosques
    #print np.mean(riqm) #riqueza promedio en matriz
    #promtot=float(np.sum(riqb)+np.sum(riqm))/(x_celdas*y_celdas)
    #print promtot #riqueza promedio total
    ##print "riq beta", np.sum(riqueza)/promtot #riqueza beta
    
    #riqueza=[]
    #promedios=[]
    #for idx in range(n_especies):
    #    promedio=np.mean(np.sum(np.sum(poblacion[-200:,:,:,idx],1),1)) #promedio de cada especie en las últimas 200 iteraciones
    #    #print promedio
    #    promedios.append(promedio) #pone el promedio de cada especie en una lista
    #    if np.mean(np.sum(np.sum(poblacion[-200:,:,:,idx],1),1))>0.0001: riqueza.append(1)  #si el promedio de cada sp está arriba de 0.0001, agrega 1 a la lista "riqueza"
    #print  np.sum(promedios) #suma promedio de las especies con las últimas 200 iteraciones
    #sumbosques=np.mean(np.sum(poblacion[-200:,1,1,:],1))+np.mean(np.sum(poblacion[-200:,1,8,:],1))+np.mean(np.sum(poblacion[-200:,5,5,:],1))+np.mean(np.sum(poblacion[-200:,8,1,:],1))+np.mean(np.sum(poblacion[-200:,8,8,:],1)) #suma total en los 5 bosques en la última iteración
    #print sumbosques #sum(x) bosques
    #print np.sum(promedios)-sumbosques #sum(x) matriz
    #print np.sum(riqueza) 
    ##poblacionpromedio=np.mean(poblacion[-200:,:,:,:],0) #promedia sobre eje tiempo: arroja array de forma (10,10,10) con las poblaciones promedio de las últimas 200 iteraciones
    ##shannon = alpha_shannon(poblacionpromedio)
    ##print "shannon", shannon
    ##print "nefectivo", nefectivo(shannon)
    #
    ##riqueza promedio en bosques y matriz y alfa y beta 
    #riqb=[] #aquí irá la riqueza en cada celda bosque para depués promediar
    #riqm=[] #aquí irá la riqueza en cada celda matriz para depués promediar
    #for i in range(len(tipo)):
    #    for j in range(len(tipo)):
    #        if tipo[i][j] == 'b':
    #            riquezas=(np.mean(poblacion[-200:,i,j,:],0)>0.001).sum() #usa array promediado en eje tiempo
    #            riqb.append(riquezas)
    #        else:
    #            riquezas2=(np.mean(poblacion[-200:,i,j,:],0)>0.001).sum() #usa array promediado en eje tiempo
    #            riqm.append(riquezas2)
    #print np.mean(riqb) #riqueza promedio en bosques
    #print np.mean(riqm) #riqueza promedio en matriz
    #promtot=float(np.sum(riqb)+np.sum(riqm))/(x_celdas*y_celdas)
    #print promtot #riqueza promedio total
    ##print np.sum(riqueza)/promtot #riqueza beta
    #
    #
    
    numerodemilpas-=5

"""